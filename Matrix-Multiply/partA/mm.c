/* matrix_multiplier (mm)
 *
 * @author Kevin Stein
 * @author Christopher Patton
 */

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <float.h>

#define USAGE_MSG "usage: matrix <file1> <file2>\n"
#define FILE_MSG "one of the files is invalid\n"
#define OUT_FILE "./result.out"
#define DFLT_BF_SIZE 512
#define DFLT_STR_SIZE 2048
#define INIT_ROW_SIZE 1024

char * readLongLine(int fd);
char * trimWhitespace(char *str);
float * strToFArray(char *str, int * cols);
float ** getMatrix(int fd, int *dims);
float ** multiply(float **m1, int dim11, int dim12,
 float **m2, int dim21, int dim22);
void writeMatrix(float **m, int dim1, int dim2, int fd);

int main (int argc, char **argv) {
   int fd1, fd2, fd3, dims[2][2];
   float **m1, **m2;
   float **m3;

   if (argc < 3) {
      fprintf(stderr, USAGE_MSG);
      exit(EXIT_FAILURE);
   }

   fd1 = open(argv[1], O_RDONLY);
   fd2 = open(argv[2], O_RDONLY);

   if(fd1 < 0 || fd2 < 0) {
      fprintf(stderr, FILE_MSG);
      exit(EXIT_FAILURE);
   }

   m1 = getMatrix(fd1, dims[0]);
   close(fd1);

   m2 = getMatrix(fd2, dims[1]);
   close(fd2);

   if ((m3 = multiply(m1, dims[0][0], dims[0][1],
    m2, dims[1][0], dims[1][1]))) {
      fd3 = open(OUT_FILE, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP);
      writeMatrix(m3, dims[0][0], dims[1][1], fd3);
      free(m3);
      close(fd3);
   }
   else fprintf(stderr, "invalid matrix dimensions\n");

   free(m1);
   free(m2);

   return 0;
}

/*
 * unbuffered input: dynamically allocates enough space for single line of input
 */
char * readLongLine(int fd) {
   char bf[DFLT_BF_SIZE], *str = (char*)malloc(sizeof(char) * DFLT_BF_SIZE);
   int ndx1, ndx2, size = DFLT_STR_SIZE, length;

   // read and store in str until str is full or file is empty
   for (ndx1 = 0, str[0] = '\0';
    DFLT_BF_SIZE == (length = read(fd, bf, DFLT_BF_SIZE)); ndx1 += length) {
      if (size - ndx1 < length)
         str = (char*)realloc(str, (size *= 2));
      strcpy((char*)(str + ndx1), bf);

      for (ndx2 = 0; ndx2 < length; ++ndx2) {
         if (bf[ndx2] == '\n') {
            lseek(fd, ndx2 - length + 1, SEEK_CUR);

            str[ndx1 + ndx2] = '\0';
            str = (char*)realloc(str, ndx1 + ndx2 + 1);

            str = trimWhitespace(str);

            return str;
         }
      }
   }

   // store last part of file read into str
   if (size - ndx1 < length)
      str = (char*)realloc(str, (size *= 2));
   strcpy((char*)(str + ndx1), bf);

   for (ndx2 = 0; ndx2 < length; ++ndx2) {
      if (bf[ndx2] == '\n') {
         lseek(fd, ndx2 - length + 1, SEEK_CUR);

         str[ndx1 + ndx2] = '\0';
         str = (char*)realloc(str, ndx1 + ndx2 + 1);

         str = trimWhitespace(str);

         return str;
      }
   }

   ndx1 += length;

   // cleanup
   if (size != ndx1)
      str = (char*)realloc(str, ndx1+1);
   str[ndx1] = '\0';

   str = trimWhitespace(str);

   return str;
}

/*
 * removes leading, extraneous inner, and trailing whitespace
 */
char * trimWhitespace(char *str) {
   int ndx = 0, shift = 0;

   // leading
   while (str[ndx + shift] == ' ') ++shift;

   //inner and trailing
   while (str[ndx + shift]) {
      if (str[ndx + shift] == ' ' && str[ndx + shift - 1] == ' ') ++shift;
      else {
         str[ndx] = str[ndx + shift];
         ++ndx;
      }
   }

   // final space
   if (str[ndx + shift - 1]) str[ndx - 1] = '\0';
   else str[ndx] = '\0';

   return str;
}

/*
 * converts space- or newline-delimited string to a float array
 */
float * strToFArray(char *str, int *cols) {
   int length = INIT_ROW_SIZE, size = 0;
   char *curr, *end = str + strlen(str);
   float *dArr = (float*)malloc(sizeof(float) * INIT_ROW_SIZE);

   curr = strtok(str, " \n");
   if (curr) {
      dArr[0] = atof(curr);
      ++size;

      while (str < end && (curr = strtok(NULL, " \n"))) {
         if (length == size)
            dArr = (float*)realloc(dArr, sizeof(float) * (length *= 2));

         dArr[size++] = atof(curr);
      }
   }
   if (size != length) dArr = (float*)realloc(dArr, sizeof(float) * size);
   *cols = size;

   return dArr;
}

/*
 * translates file of numerical strings to 2-D float array by calling:
 * readLongLine, strToFArray
 */
float ** getMatrix(int fd, int *dims) {
   char *line = readLongLine(fd);
   int rows = 0, cols= 0, oldcol = 0, memrows = INIT_ROW_SIZE;
   float ** matrix = malloc(sizeof(float *) * memrows);

   while(strlen(line)) {
      ++rows;
      if(rows > memrows) {
         memrows = memrows * 2;
         matrix = (float **) realloc(matrix, sizeof(float *) * memrows);
      }
      oldcol = cols;
      matrix[rows - 1] = strToFArray(line, &cols);
      if(cols != oldcol && oldcol) {}
      free(line);
      line = readLongLine(fd);
   }

   dims[0] = rows;
   dims[1] = cols;
   if (rows < memrows)
      matrix = (float **) realloc(matrix, sizeof(float *) * rows);

   return matrix;
}

/*
 * computes and returns dot procuct of two matrices
 */
float ** multiply(float **m1, int dim11, int dim12,
 float **m2, int dim21, int dim22) {
   int ndx1, ndx2, ndx3;
   float **matrix;

   if (dim12 != dim21)
      return NULL;

   matrix = (float**)malloc(sizeof(float*) * dim11);
   for (ndx1 = 0; ndx1 < dim11; ++ndx1)
      matrix[ndx1] = (float*)malloc(sizeof(float) * dim22);

   for (ndx1 = 0; ndx1 < dim11; ++ndx1)
      for (ndx2 = 0; ndx2 < dim22; ++ndx2)
         for (ndx3 = 0, matrix[ndx1][ndx2] = 0.0; ndx3 < dim12; ++ndx3)
            matrix[ndx1][ndx2] += m1[ndx1][ndx3] * m2[ndx3][ndx2];

   return matrix;
}

/*
 * unbuffered output: writes values of matrix to file
 */
void writeMatrix(float **m, int dim1, int dim2, int fd) {
   int length, size, ndx1, ndx2, max_dbl_chars = 4 + DBL_MANT_DIG - DBL_MIN_EXP;
   char str_bf[DFLT_STR_SIZE + 1],
    *num_bf = (char*)alloca(sizeof(char) * max_dbl_chars);

   str_bf[0] = num_bf[0] = '\0';
   size = 0;

   for (ndx1 = 0; ndx1 < dim1; ++ndx1) {
      for (ndx2 = 0; ndx2 < dim2; ++ndx2) {
         length = snprintf(num_bf, max_dbl_chars, "%.2lf%c", m[ndx1][ndx2],
          (ndx2 < dim2 - 1 ? ' ' : '\n'));

         strncpy((char*)(str_bf + size), num_bf, DFLT_STR_SIZE - size + 1);
         if (size + length > DFLT_STR_SIZE) {
            write(fd, str_bf, size);
            strncpy(str_bf, (num_bf + (DFLT_STR_SIZE - size)),
             (size = (length - (DFLT_STR_SIZE - size))) + 1);
         }
         else size += length;
      }
   }

   write(fd, str_bf, size);
}

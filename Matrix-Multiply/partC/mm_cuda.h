#ifndef MM_CUDA_H
#define MM_CUDA_H

#include <cuda.h>

#include "util.h"

#define TW 16

#ifdef __cplusplus
extern "C" {
   void mm_cuda(float *m1, float *m2, float *m3, int dim1, int dim2, int dim3);
   __global__ void mmkernel(float *d_m1, float *d_m2, float *d_m3,
    int dim1, int dim2, int dim0);
}
#endif

#endif

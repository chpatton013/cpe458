#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>

#define CALL_ERR(funct, err) \
   if ((funct) == (err)) { \
      perror(__FILE__); \
      exit(EXIT_FAILURE); \
   }

#define CALL_WARN(funct, err) \
   if ((funct) == (err)) { \
      perror(__FILE__); \
   }

#endif

#ifndef MM_H
#define MM_H

/* matrix_multiplier (mm)
 *
 * @author Kevin Stein
 * @author Christopher Patton
 */

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <float.h>

#include "util.h"
#include "mm_cuda.h"

#define USAGE_MSG "usage: matrix <file1> <file2> [-c|-g]\n"
#define OUT_FILE "./result.out"
#define DFLT_BF_SIZE 2048
#define DFLT_STR_SIZE 4096
#define INIT_ROW_SIZE 1024
#define GPU_THRESHOLD 250000

char mode;

char * readLongLine(FILE *fp);
char * trimWhitespace(char *str);
float * strToFArray(char *str, int * cols);
float * getMatrix(FILE *fp, int *dims);
float * multiply(float *m1, int dim11, int dim12,
 float *m2, int dim21, int dim22);
void mm(float *m1, float *m2, float *m3, int dim1, int dim2, int dim0);
void writeMatrix(float *m, int dim1, int dim2, FILE *fp);

#endif

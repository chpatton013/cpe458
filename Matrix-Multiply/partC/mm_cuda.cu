#include "mm_cuda.h"

void mm_cuda(float *m1, float *m2, float *m3, int dim1, int dim2, int dim0) {
   float *d_m1, *d_m2, *d_m3;
   dim3 gridDim(dim2 / TW + ((dim2 % TW) ? 1 : 0),
                dim1 / TW + ((dim1 % TW) ? 1 : 0)),
        blockDim(TW, TW);

   cudaMalloc((void**)&d_m1, dim1 * dim0 * sizeof(float));
   cudaMalloc((void**)&d_m2, dim0 * dim2 * sizeof(float));
   cudaMalloc((void**)&d_m3, dim1 * dim2 * sizeof(float));

   cudaMemcpy(d_m1, m1, dim0 * dim1 * sizeof(float), cudaMemcpyHostToDevice);
   cudaMemcpy(d_m2, m2, dim2 * dim0 * sizeof(float), cudaMemcpyHostToDevice);

   mmkernel<<<gridDim,blockDim>>>(d_m1, d_m2, d_m3, dim1, dim2, dim0);

   cudaMemcpy(m3, d_m3, dim1 * dim2 * sizeof(float), cudaMemcpyDeviceToHost);

   cudaFree(d_m1);
   cudaFree(d_m2);
   cudaFree(d_m3);
}

__global__ void mmkernel(float *d_m1, float *d_m2, float *d_m3,
 int dim1, int dim2, int dim0) {
   register int ndx1, ndx2, blocks, temp,
    row = blockIdx.y * TW + threadIdx.y,
    col = blockIdx.x * TW + threadIdx.x;
   register float val = 0.0;
   __shared__ float ds_m1[TW][TW], ds_m2[TW][TW];

   blocks = dim0 / TW + (dim0 % TW ? 1 : 0);
   for (ndx1 = 0; ndx1 < blocks; ++ndx1) {
      temp = ndx1 * TW;

      ds_m1[threadIdx.y][threadIdx.x] =
       d_m1[row * dim0 + temp + threadIdx.x];

      ds_m2[threadIdx.y][threadIdx.x] =
       d_m2[col + (temp + threadIdx.y) * dim2];

      __syncthreads();

      for (ndx2 = 0; ndx2 < TW; ++ndx2)
         val += temp + ndx2 >= dim0 ? 0.0 :
          ds_m1[threadIdx.y][ndx2] * ds_m2[ndx2][threadIdx.x];

      __syncthreads();
   }

   if (row < dim1 && col < dim2)
      d_m3[row * dim2 + col] = val;
}

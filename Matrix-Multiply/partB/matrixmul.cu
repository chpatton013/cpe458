/* matrix_multiplier (mm)
 *
 * @author Kevin Stein
 * @author Christopher Patton
 */

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <cuda.h>

#define USAGE_MSG "usage: matrix <file1> <file2>\n"
#define FILE_MSG "one of the files is invalid\n"
#define OUT_FILE "./result.out"
#define DFLT_BF_SIZE 2048
#define DFLT_STR_SIZE 4096
#define INIT_ROW_SIZE 1024

#define THRDS_PER_BLK 256
#define THRDS_PER_DIM 16

#define CALL_ERR(funct, err) \
   if ((funct) == (err)) { \
      perror(__FILE__); \
      exit(EXIT_FAILURE); \
   }

#define CALL_WARN(funct, err) \
   if ((funct) == (err)) { \
      perror(__FILE__); \
   }

char * readLongLine(FILE *fp);
char * trimWhitespace(char *str);
float * strToFArray(char *str, int * cols);
float * getMatrix(FILE *fp, int *dims);
float * multiply(float *m1, int dim11, int dim12,
 float *m2, int dim21, int dim22);
__global__ void mmkernel(float *d_m1, int dim11, int dim12, float *d_m2,
 int dim21, int dim22, float *d_m3);
void writeMatrix(float *m, int dim1, int dim2, FILE *fp);

int main (int argc, char **argv) {
   int dims[2][2];
   float *m1, *m2, *m3;
   FILE *fpi1, *fpi2, *fpo;

   if (argc < 3) {
      fprintf(stderr, USAGE_MSG);
      exit(EXIT_FAILURE);
   }

   CALL_ERR((fpi1 = fopen(argv[1], "r")), NULL);
   CALL_ERR((fpi2 = fopen(argv[2], "r")), NULL);

   m1 = getMatrix(fpi1, dims[0]);
   fclose(fpi1);

   m2 = getMatrix(fpi2, dims[1]);
   fclose(fpi2);

   if ((m3 = multiply(m1, dims[0][0], dims[0][1],
    m2, dims[1][0], dims[1][1]))) {
      fpo = fopen(OUT_FILE, "w");
      writeMatrix(m3, dims[0][0], dims[1][1], fpo);
      free(m3);
      fclose(fpo);
   }
   else fprintf(stderr, "invalid matrix dimensions\n");

   free(m1);
   free(m2);

   return 0;
}

/*
 * buffered input: dynamically allocates enough space for single line of input
 */
char * readLongLine(FILE *fp) {
   char bf[DFLT_BF_SIZE];
   char *str;
   int ndx, size, length;

   if (!fp) return NULL;

   CALL_ERR((str = (char*)malloc(DFLT_STR_SIZE * sizeof(char))), NULL);
   size = 0;
   length = DFLT_STR_SIZE;

   while (fgets(bf, DFLT_BF_SIZE, fp)) {
      for (ndx = 0; bf[ndx]; ++ndx) {
         if (size + 1 == length) {
            length *= 2;
            CALL_ERR((str = (char*)realloc(str, length * sizeof(char))), NULL);
         }

         str[size++] = bf[ndx];
      }

      if (str[size-1] == '\n') {
         str[size-1] = '\0';
         break;
      }
   }

   str[size] = '\0';

   return str;
}

/*
 * removes leading, extraneous inner, and trailing whitespace
 */
char * trimWhitespace(char *str) {
   int ndx = 0, shift = 0;

   // leading
   while (str[ndx + shift] == ' ') ++shift;

   //inner and trailing
   while (str[ndx + shift]) {
      if (str[ndx + shift] == ' ' && str[ndx + shift - 1] == ' ') ++shift;
      else {
         str[ndx] = str[ndx + shift];
         ++ndx;
      }
   }

   // final space
   if (str[ndx + shift - 1]) str[ndx - 1] = '\0';
   else str[ndx] = '\0';

   return str;
}

/*
 * converts space- or newline-delimited string to a float array
 */
float * strToFArray(char *str, int *cols) {
   int length = INIT_ROW_SIZE, size = 0;
   char *curr, *end = str + strlen(str);
   float *dArr = (float*)malloc(sizeof(float) * INIT_ROW_SIZE);

   curr = strtok(str, " \n");
   if (curr) {
      dArr[0] = atof(curr);
      ++size;

      while (str < end && (curr = strtok(NULL, " \n"))) {
         if (length == size)
            CALL_ERR((dArr = (float*)realloc(dArr, sizeof(float) * (length *= 2))), NULL);

         dArr[size++] = atof(curr);
      }
   }

   *cols = size;

   return dArr;
}

/*
 * translates file of numerical strings to 2-D float array by calling:
 * readLongLine, strToFArray
 */
float * getMatrix(FILE *fp, int *dims) {
   char *line = readLongLine(fp);
   int values = 0, initvals = INIT_ROW_SIZE, cols = 0, //oldcol = 0,
       valpointer = 0, rows = 0, z;
   float * matrix = (float *)malloc(sizeof(float) * initvals);
   float * tempmatrix;

   trimWhitespace(line);
   while(strlen(line)) {
      tempmatrix = strToFArray(line, &cols);
      values += cols;

      for(z = 0; z < cols; z++) {
         if(values > initvals) {
            initvals = initvals * 2;
            CALL_ERR((matrix = (float*)realloc(matrix, sizeof(float) * initvals)), NULL);
         }

         matrix[valpointer++] = tempmatrix[z];
      }

      ++rows;

      free(tempmatrix);
      free(line);
      line = readLongLine(fp);
      trimWhitespace(line);
   }

   dims[0] = rows;
   dims[1] = cols;

   return matrix;
}

/*
 * computes and returns dot procuct of two matrices
 */
float * multiply(float *m1, int dim11, int dim12,
 float *m2, int dim21, int dim22) {
   float *m3, *d_m1, *d_m2, *d_m3;

   if (dim12 != dim21)
      return NULL;

   m3 = (float*)malloc(sizeof(float) * dim11 * dim22);

   cudaMalloc((void**)&d_m1, sizeof(float) * dim11 * dim12);
   cudaMalloc((void**)&d_m2, sizeof(float) * dim21 * dim22);
   cudaMalloc((void**)&d_m3, sizeof(float) * dim11 * dim22);

   cudaMemcpy(d_m1, m1, sizeof(float) * dim12 * dim11,
    cudaMemcpyHostToDevice);

   cudaMemcpy(d_m2, m2, sizeof(float) * dim22 * dim21,
    cudaMemcpyHostToDevice);

   dim3 gridDim(max(dim11 * dim22 / THRDS_PER_BLK, 1), 1);
   dim3 blockDim(THRDS_PER_BLK, 1, 1);

   mmkernel<<<gridDim,blockDim>>>(d_m1, dim11, dim12, d_m2, dim21, dim22, d_m3);

   cudaMemcpy(m3, d_m3, dim11 * dim22 * sizeof(float),
    cudaMemcpyDeviceToHost);

   cudaFree(d_m1);
   cudaFree(d_m2);
   cudaFree(d_m3);

   return m3;
}

__global__ void mmkernel(float *d_m1, int dim11, int dim12, float *d_m2,
 int dim21, int dim22, float *d_m3) {
   int index, x = (blockIdx.x * THRDS_PER_BLK + threadIdx.x) / dim22,
    y = threadIdx.x % dim22 + (blockIdx.x % (gridDim.x < dim11) ? 1 :
    gridDim.x / dim11) * THRDS_PER_BLK;
   float sum = 0.0;

   if (x < dim11 && y < dim22) {
      for (index = 0; index < dim12; ++index)
         sum += d_m1[x * dim12 + index] * d_m2[y + dim22 * index];

      d_m3[blockIdx.x * THRDS_PER_BLK + threadIdx.x] = sum;
   }
}

/*
 * buffered output: writes values of matrix to file
 */
void writeMatrix(float *m, int dim1, int dim2, FILE *fp) {
   int ndx1, ndx2;

   for (ndx1 = 0; ndx1 < dim1; ++ndx1)
      for (ndx2 = 0; ndx2 < dim2; ++ndx2)
         fprintf(fp, "%.2lf%c", m[ndx1 * dim2 + ndx2], (ndx2 < dim2 - 1 ? ' ' : '\n'));
}

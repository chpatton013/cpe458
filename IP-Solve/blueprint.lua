-- Blueprint file for cubuild
-- See http://github.com/bobsomers/cubuild for more info

-- Where can I find the GPU Computing SDK?
-- gpu_sdk_path "/tmp/gpusdk"
gpu_sdk_path "/tmp2/gpusdk"
-- gpu_sdk_path "/usr/local/cuda-sdk"

-- -- Which configuration should be the default?
default "release"

-- The debug build configuration.
config "debug"
   output "build-debug"
   compute_capability "2.0"
   debugging "on"
   profiling "off"
   optimizing "off"
   defines {"DEBUG"}

-- The profiling build configuration.
config "profile"
   output "build-profile"
   compute_capability "2.0"
   debugging "on"
   profiling "on"
   optimizing "on"
   defines {"DEBUG"}

-- The release build configuration.
config "release"
   output "build-release"
   compute_capability "2.0"
   debugging "off"
   profiling "off"
   optimizing "on"
   defines {"NDEBUG", "RELEASE"}
   includes {}
   cflags {} --"-Wall", "-ffast_math"}
   libdirs {}
   libs {} --"m", "boost_regex"}
   lflags {} --"-m64"}

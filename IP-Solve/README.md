CUDA-IPSolver
=============

CUDA-IPSolver optimizes integer programming models for an arbitrary number of
constraints and objective functions in Nth-dimensional space.

Compilation:

How to get cubuild
  - Download cubuild via git [ 'git clone git@github.com:bobsomers/cubuild.git' ]
  - Compile cubuild from source [ 'make' ]
  - Move the cubuild executable to ~/bin
  - Add the following to your .bashrc file (or equivalent):
     export PATH=$PATH:/opt/cuda/bin
     export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cuda/lib64:/opt/cuda/lib
  - Run the above exports in your current shell or restart the shell.

How to compile
  - Compile your source using cubuild [ 'cubuild' or 'cubuild -verbose' ]

How to clean up
  - Remove objects and binaries using cubuild [ 'cubuild clean' ]

Usage:

 executable [ c constraints ] [ o objectives ] [ b boundaries ]

Behavior:

 CUDA-IPSolver reads up to three different files to obtain constraint and
 objective expressions, and artificial boundaries. The following formats must
 be strictly followed:

Constraints:
  - Each line must contain a complete post-fix expression, followed by a
    comparator flag as an integer (1 = Equality, 2 = Less than, 4 = Greater
    than).

Objectives:
  - Each line must contain a complete post-fix expression.

Boundaries:
  - Each line must contain a pair of exclusive integer boundaries for the
    dimension that corresponds to the line number (line 1 bounds dimension 1,
    line 2 bounds dimension 2, etc).

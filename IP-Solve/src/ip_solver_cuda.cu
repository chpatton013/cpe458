#include "ip_solver_cuda.h"

void solve_cuda(constraint_t *cons, uint32_t num_constraints,
      objective_t *objs, uint16_t num_objectives, int32_t *bounds,
      result_t *res) {
   int32_t *d_bounds, ranges[MAX_NUM_DIMS], *d_ranges, partial_temp;
   uint16_t *d_n;
   volatile uint16_t *d_max_ndx;
   uint32_t ndx, partials[MAX_NUM_DIMS], *d_partials;
   uint64_t num_points, blocks_remaining, curr_num_blocks;
   volatile uint64_t *d_obj_lock;
   dim3 blocks(1,1), threads(1,1,1);
   constraint_t *d_cons;
   objective_t *d_objs;

   if (!(cons && objs && bounds && res)) return;

   // initialize constraints
   CALL_N_ERR(cudaMalloc((void**)&d_cons, sizeof(constraint_t) *
            num_constraints), cudaSuccess);
   CALL_N_ERR((cudaMemcpy(d_cons, cons, sizeof(constraint_t) * num_constraints,
         cudaMemcpyHostToDevice)), cudaSuccess);

   // initialize objectives
   CALL_N_ERR(cudaMalloc((void**)&d_objs, sizeof(objective_t) * num_objectives),
         cudaSuccess);
   CALL_N_ERR((cudaMemcpy((void*)d_objs, objs, sizeof(objective_t) *
         num_objectives, cudaMemcpyHostToDevice)), cudaSuccess);

   // find number of dimensions
   // initialize n
   CALL_N_ERR(cudaMalloc((void**)&d_n, sizeof(uint16_t)),
         cudaSuccess);

   // calculate necessary grid model
   threads.x = TPB;
   if (num_constraints / TPB < MAX_BLK_DIM)
      blocks.x = num_constraints / TPB + (num_constraints % TPB ? 1 : 0);
   else {
      blocks.x = MAX_BLK_DIM;
      blocks.y = (num_constraints / TPB) / MAX_BLK_DIM +
            ((num_constraints / TPB) % MAX_BLK_DIM ? 1 : 0);
   }

   // find num dimensions
   get_num_dims_cuda <<<blocks,threads>>> (d_cons, num_constraints, d_n);

   // copy back and cleanup
   CALL_N_ERR(cudaMemcpy(&n, d_n, sizeof(uint16_t), cudaMemcpyDeviceToHost),
         cudaSuccess);
   CALL_N_ERR(cudaFree(d_n), cudaSuccess);

   // run optimization
   if (n <= MAX_NUM_DIMS && n > 0) {
      // initialize objective lock
      CALL_N_ERR(cudaMalloc((void**)&d_obj_lock, sizeof(uint64_t) *
            MAX_NUM_OBJS), cudaSuccess);

      // initialize max_ndx
      CALL_N_ERR(cudaMalloc((void**)&d_max_ndx, sizeof(uint16_t)), cudaSuccess);

      // initialize global memory values
      init_global_memory <<<1,TPB>>> (d_max_ndx, d_objs, d_obj_lock, num_objectives);

      // initialize bounds
      CALL_N_ERR(cudaMalloc((void**)&d_bounds, sizeof(int32_t) * MAX_NUM_DIMS *
            2), cudaSuccess);
      CALL_N_ERR((cudaMemcpy(d_bounds, bounds, sizeof(int32_t) * MAX_NUM_DIMS *
            2, cudaMemcpyHostToDevice)), cudaSuccess);

      // calculate and initialize ranges
      for (ndx = 0; ndx < MAX_NUM_DIMS; ++ndx)
         ranges[ndx] = bounds[ndx * 2 + 1] - bounds[ndx * 2] + 1;
      CALL_N_ERR(cudaMalloc((void**)&d_ranges, sizeof(int32_t) * MAX_NUM_DIMS),
            cudaSuccess);
      CALL_N_ERR((cudaMemcpy(d_ranges, ranges, sizeof(int32_t) * MAX_NUM_DIMS,
            cudaMemcpyHostToDevice)), cudaSuccess);

      // calculate and initialize partials
      partial_temp = 1;
      for (ndx = 1; ndx < n; ++ndx) {
         partial_temp *= ranges[ndx];
         partials[MAX_NUM_DIMS - ndx - 1] = partial_temp;
      }
      partials[n - 1] = 1;
      CALL_N_ERR(cudaMalloc((void**)&d_partials, sizeof(int32_t) * MAX_NUM_DIMS),
            cudaSuccess);
      CALL_N_ERR((cudaMemcpy(d_partials, partials, sizeof(int32_t) * MAX_NUM_DIMS,
            cudaMemcpyHostToDevice)), cudaSuccess);

      // determine the number of blocks remaining
      for (ndx = 0, num_points = 1; ndx < n; ++ndx)
         num_points *= (bounds[ndx * 2 + 1] - bounds[ndx * 2] + 1);

      blocks_remaining = num_points / TPB + (num_points % TPB ? 1 : 0);

      for (ndx = 0; blocks_remaining > 0; ++ndx) {
         // calculate necessary grid model
         threads.x = TPB;
         if (blocks_remaining / MAX_BLK_DIM) {
            if ((blocks_remaining / MAX_BLK_DIM) / MAX_BLK_DIM) {
               blocks.x = MAX_BLK_DIM;
               blocks.y = MAX_BLK_DIM;
            }
            else {
               blocks.x = MAX_BLK_DIM;
               blocks.y = blocks_remaining / MAX_BLK_DIM +
                     (blocks_remaining % MAX_BLK_DIM ? 1 : 0);
            }
         }
         else {
            blocks.x = blocks_remaining;
            blocks.y = 1;
         }

         // optimize
         optimize_cuda <<<blocks,threads>>> (d_cons, num_constraints, d_objs,
               d_obj_lock, num_objectives, d_bounds, d_ranges, d_partials, n,
               d_max_ndx, ndx);

         // bookkeeping
         curr_num_blocks = blocks.x * blocks.y;
         blocks_remaining = blocks_remaining > curr_num_blocks ?
               blocks_remaining - curr_num_blocks : 0;
      }

      // transfer objectives back
      CALL_N_ERR((cudaMemcpy(objs, (void*)d_objs, sizeof(objective_t) *
            num_objectives, cudaMemcpyDeviceToHost)), cudaSuccess);

      // transfer max_ndx back and update result
      CALL_N_ERR(cudaMemcpy(&res->max_ndx, (void*)d_max_ndx, sizeof(uint16_t),
            cudaMemcpyDeviceToHost), cudaSuccess);
      res->obj = &objs[res->max_ndx];

      // cleanup
      CALL_N_ERR(cudaFree((void*)d_obj_lock), cudaSuccess);
      CALL_N_ERR(cudaFree((void*)d_max_ndx), cudaSuccess);
      CALL_N_ERR(cudaFree(d_bounds), cudaSuccess);
      CALL_N_ERR(cudaFree(d_ranges), cudaSuccess);
      CALL_N_ERR(cudaFree(d_partials), cudaSuccess);
   }
   else {
      fprintf(stderr, DIMENSION_MSG);
      exit(EXIT_FAILURE);
   }

   // cleanup
   CALL_N_ERR(cudaFree(d_cons), cudaSuccess);
   CALL_N_ERR(cudaFree((void*)d_objs), cudaSuccess);
}

__global__ void get_num_dims_cuda(constraint_t *d_cons,
      uint32_t num_constraints, uint16_t *num_dimensions) {
   uint32_t tid, ndx;
   uint16_t data, num_nodes = d_cons->expr.num_nodes;
   __shared__ uint16_t max_dim;

   tid = (blockIdx.y * gridDim.x * gridDim.y + blockIdx.x * gridDim.x) *
         blockDim.x + threadIdx.x;

   if (tid == 0) *num_dimensions = 0;
   if (tid < num_constraints && threadIdx.x == 0) max_dim = 0;

   __syncthreads();

   if (tid < num_constraints) {
      // maximuze shared value
      for (ndx = 0; ndx < num_nodes; ++ndx) {
         if (d_cons[tid].expr.nodes[ndx + 1].type == VARIABLE) {
            data = (uint16_t)d_cons[tid].expr.nodes[ndx + 1].data;
            while (data > max_dim) max_dim = data;
         }
      }
   }

   __syncthreads();

   if (tid < num_constraints && threadIdx.x == 0)
      // maximize global value
      while (max_dim > *num_dimensions) *num_dimensions = max_dim;
}

__global__ void init_global_memory(volatile uint16_t *d_max_ndx,
      objective_t *d_objs, volatile uint64_t *d_obj_lock,
      uint16_t num_objectives) {
   if (blockIdx.x == 0 && blockIdx.y == 0)
   {
      if (threadIdx.x == 0)
         *d_max_ndx = 0;
      if (threadIdx.x < num_objectives) {
         d_objs[threadIdx.x].value = FLT_MIN;
         d_obj_lock[threadIdx.x] = unlocked;
      }
   }
}

__global__ void optimize_cuda(constraint_t *d_cons, uint32_t num_constraints,
      objective_t *d_objs, volatile uint64_t *d_obj_lock,
      uint16_t num_objectives, int32_t *d_bounds, int32_t *d_ranges,
      uint32_t *d_partials, uint16_t num_dimensions,
      volatile uint16_t *d_max_ndx, uint32_t iter) {
   unsigned char cmp, feasible;
   uint32_t ndx1, ndx2;
   uint64_t point_ndx, point_temp;
   float val, point[MAX_NUM_DIMS];
   __shared__ int32_t ds_bounds[MAX_NUM_DIMS * 2];
   __shared__ volatile uint16_t ds_max_ndx;
   __shared__ uint32_t ds_partials[MAX_NUM_DIMS];
   __shared__ volatile uint64_t ds_max_ndx_lock, ds_obj_lock[MAX_NUM_OBJS];
   __shared__ volatile float ds_max_val;
   __shared__ objective_t ds_objs[MAX_NUM_OBJS];

   // setup shared memory
   if (threadIdx.x == 0)
   {
      ds_max_val = FLT_MIN;
      ds_max_ndx = 0;
      ds_max_ndx_lock = unlocked;
   }
   if (threadIdx.x < MAX_NUM_DIMS)
   {
      ds_obj_lock[threadIdx.x] = unlocked;
      ds_partials[threadIdx.x] = d_partials[threadIdx.x];
   }
   if (threadIdx.x < 2 * MAX_NUM_DIMS)
      ds_bounds[threadIdx.x] = d_bounds[threadIdx.x];
   if (threadIdx.x < num_objectives)
   {
      ds_objs[threadIdx.x].value = FLT_MIN;
      for (ndx1 = 0; ndx1 < MAX_NUM_DIMS; ++ndx1)
         ds_objs[threadIdx.x].point[ndx1] = 0;
      ds_objs[threadIdx.x].expr = d_objs[threadIdx.x].expr;
   }

   __syncthreads();

   // determine which point this thread is evaluating
   point_temp = point_ndx = iter * gridDim.x * gridDim.y +
         (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
   for (ndx1 = 0; ndx1 < num_dimensions; ++ndx1)
   {
      point[ndx1] = (int32_t)(point_temp / (uint64_t)ds_partials[ndx1]) +
            ds_bounds[ndx1 * 2];
      point_temp %= (uint64_t)ds_partials[ndx1];
   }

   // is point in feasible region?
   feasible = 1;
   for (ndx1 = 0; ndx1 < num_constraints; ++ndx1)
   {
      val = compute_expr(&d_cons[ndx1].expr, point);
      cmp = d_cons[ndx1].cmp;
      feasible &= (val == 0.0f && cmp & CMP_EQUAL_TO) ||
            (val < 0.0f && cmp & CMP_LESS_THAN) ||
            (val > 0.0f && cmp & CMP_GREATER_THAN);
   }

   // evaluate point in shared memory
   if (feasible)
   {
      for (ndx1 = 0; ndx1 < num_objectives; ++ndx1)
      {
         val = compute_expr(&ds_objs[ndx1].expr, point);
         while (val > ds_objs[ndx1].value)
         {
            if (ds_obj_lock[ndx1] == unlocked)
            {
               atomicExch((ulli*)&ds_obj_lock[ndx1], (ulli)point_ndx);
               __threadfence_block();
               if (ds_obj_lock[ndx1] == point_ndx)
               {
                  atomicExch((float*)&ds_objs[ndx1].value, (float)val);
                  for (ndx2 = 0; ndx2 < num_dimensions; ++ndx2)
                     atomicExch((float*)&ds_objs[ndx1].point[ndx2],
                           (float)point[ndx2]);
                  atomicExch((ulli*)&ds_obj_lock[ndx1], (ulli)unlocked);
                  __threadfence_block();
               }
            }
         }
      }
   }

   __syncthreads();

   // update global objectives
   if (threadIdx.x == 0)
   {
      for (ndx1 = 0; ndx1 < num_objectives; ++ndx1)
      {
         val = ds_objs[ndx1].value;
         while (val > d_objs[ndx1].value/**/) //*/&& count < 25)
         {
            if (d_obj_lock[ndx1] == unlocked)
            {
               atomicExch((ulli*)&d_obj_lock[ndx1], (ulli)point_ndx);
               __threadfence();
               if (d_obj_lock[ndx1] == point_ndx)
               {
                  atomicExch((float*)&d_objs[ndx1].value, (float)val);
                  for (ndx2 = 0; ndx2 < num_dimensions; ++ndx2)
                     atomicExch((float*)&d_objs[ndx1].point[ndx2],
                           (float)ds_objs[ndx1].point[ndx2]);
                  atomicExch((ulli*)&d_obj_lock[ndx1], unlocked);
                  __threadfence();
               }
            }
         }
      }
   }

   __syncthreads();

   // report maximum index
   if (blockIdx.x == 0 && blockIdx.y == 0 && threadIdx.x < num_objectives)
   {
      val = d_objs[threadIdx.x].value;
      while (val > ds_max_val)
      {
         if (ds_max_ndx_lock == unlocked)
         {
            atomicExch((ulli*)&ds_max_ndx_lock, (ulli)point_ndx);
            __threadfence();
            if (ds_max_ndx_lock == point_ndx)
            {
               atomicExch((unsigned int*)&ds_max_ndx, threadIdx.x);
               atomicExch((float*)&ds_max_val, (float)val);
               atomicExch((ulli*)&ds_max_ndx_lock, (ulli)unlocked);
               __threadfence();
            }
         }
      }
      if (threadIdx.x == 0)
         *d_max_ndx = ds_max_ndx;
   }
}

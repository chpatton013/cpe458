#include "objective.h"

objective_t * get_objectives(FILE *obj_fp, uint16_t *num_objectives) {
   objective_t *objs;
   char *line, **strs;
   uint32_t ndx, num_strs, size, length;

   if (!obj_fp) return NULL;

   length = DFLT_NUM_OBJS;

   CALL_ERR((strs = (char**)malloc(sizeof(char*) * DFLT_NUM_OBJS)), NULL);
   for (ndx = 0; (line = read_long_line(obj_fp)); ++ndx) {
      if (ndx == length)
         CALL_ERR((strs = (char**)realloc(strs, sizeof(char*) *
               (length <<= 1))), NULL);

      strs[ndx] = line;
   }
   num_strs = ndx;

   CALL_ERR((objs = (objective_t*)malloc(sizeof(objective_t) * num_strs)),
         NULL);
   for (size = 0, ndx = 0; ndx < num_strs && size < MAX_NUM_OBJS; ++ndx)
      if (get_eq_expr(strs[ndx], &objs[ndx].expr)) {
         objs[ndx].value = FLT_MIN;
         ++size;
      }
   *num_objectives = size;

   for (ndx = 0; ndx < num_strs; ++ndx)
      if (strs[ndx]) free(strs[ndx]);
   free(strs);

   return objs;
}

inline void free_objective(objective_t *obj) {
}

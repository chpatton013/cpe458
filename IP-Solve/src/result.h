#ifndef RESULT_H
#define RESULT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include "util.h"
#include "constants.h"
#include "objective.h"

typedef struct result_t {
   uint16_t max_ndx;
   objective_t *obj;
} result_t;

#ifdef __cplusplus
}
#endif

#endif

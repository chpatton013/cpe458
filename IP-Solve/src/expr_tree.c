#include "expr_tree.h"

inline void print_node(node_t *node) {
   printf("type: %c; data: %f\n", node->type, node->data);
}

unsigned char clear_expr(expr_tree_t *expr) {
   uint16_t ndx1, ndx2;

   if (!expr) return 0;

   expr->num_nodes = 0;

   // head node
   expr->nodes[0].type = 0;
   expr->nodes[0].data = 0;
   expr->nodes[0].parent = 0;
   for (ndx1 = 0; ndx1 < MAX_NUM_CHILDREN; ++ndx1) {
      expr->nodes[0].children[ndx1] = -1;
   }

   // other nodes
   for (ndx1 = 1; ndx1 < MAX_NUM_NODES + 1; ++ndx1) {
      expr->nodes[ndx1].type = 0;
      expr->nodes[ndx1].data = 0;
      expr->nodes[ndx1].parent = -1;
      for (ndx2 = 0; ndx2 < MAX_NUM_CHILDREN; ++ndx2) {
         expr->nodes[ndx1].children[ndx2] = -1;
      }
   }

   return 1;
}

char get_eq_expr(char *str, expr_tree_t *expr) {
   node_t *stk[MAX_NUM_NODES / 2 + 1];
   uint16_t ndx1, ndx2, size;
   float val;
   char *err, op;

   if (!str || !expr) return 0;

   CALL_ERR(clear_expr(expr), 0);

   if ((str = strtok(str, " "))) {
      ndx1 = 1;
      size = 0;

      do {
         val = strtof(str, &err);
         op = get_op_type(str);

         if (str != err) {
            expr->nodes[ndx1].type = CONSTANT;
            expr->nodes[ndx1].data = val;

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else if (op < 0 && str[0] == 'x') {
            expr->nodes[ndx1].type = VARIABLE;
            expr->nodes[ndx1].data = strtod(str+1, NULL);

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else if (op >= 0) {
            expr->nodes[ndx1].type = OPERATOR;
            expr->nodes[ndx1].data = op;

            if (size < (op / NUM_OPS)) return 0;

            for (ndx2 = 0; ndx2 <= (op / NUM_OPS); ++ndx2, --size) {
               expr->nodes[ndx1].children[(op / NUM_OPS) - ndx2] =
                     stk[size-1] - expr->nodes;
               stk[size-1]->parent = ndx1;
            }

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else return 0;

         ++ndx1;
      } while ((str = strtok(NULL, " ")) && ndx1 <= MAX_NUM_NODES);

      expr->nodes[0].children[0] = stk[size-1] - expr->nodes;
      expr->num_nodes = ndx1-1;

      return 1;
   }

   return 0;
}

char get_ineq_expr(char *str, expr_tree_t *expr, unsigned char *cmp) {
   node_t *stk[MAX_NUM_NODES / 2 + 1];
   uint16_t ndx1, ndx2, size;
   float val;
   char *next, *err, op;
   unsigned char cmp_temp;

   if (!str || !expr) return 0;

   CALL_ERR(clear_expr(expr), 0);

   if ((next = strtok(str, " "))) {
      for (ndx1 = 1, size = 0; (next = strtok(NULL, " ")) && ndx1 <= MAX_NUM_NODES;
            ++ndx1, str = next) {
         val = strtof(str, &err);
         op = get_op_type(str);

         if (str != err) {
            expr->nodes[ndx1].type = CONSTANT;
            expr->nodes[ndx1].data = val;

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else if (op < 0 && str[0] == 'x') {
            expr->nodes[ndx1].type = VARIABLE;
            expr->nodes[ndx1].data = strtod(str+1, NULL);

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else if (op >= 0) {
            expr->nodes[ndx1].type = OPERATOR;
            expr->nodes[ndx1].data = op;

            if (size < (op / NUM_OPS)) return 0;

            for (ndx2 = 0; ndx2 <= (op / NUM_OPS); ++ndx2, --size) {
               expr->nodes[ndx1].children[(op / NUM_OPS) - ndx2] =
                     stk[size-1] - expr->nodes;
               stk[size-1]->parent = ndx1;
            }

            stk[size++] = &(expr->nodes[ndx1]);
         }
         else return 0;
      }

      expr->nodes[0].children[0] = stk[size-1] - expr->nodes;
      expr->num_nodes = ndx1 - 1;

      cmp_temp = 0;
      if (strstr(str, "=")) cmp_temp |= CMP_EQUAL_TO;
      if (strstr(str, "<")) cmp_temp |= CMP_LESS_THAN;
      if (strstr(str, ">")) cmp_temp |= CMP_GREATER_THAN;

      if ((cmp_temp & CMP_LESS_THAN) && (cmp_temp & CMP_GREATER_THAN)) return 0;
      else *cmp = cmp_temp;

      return 1;
   }

   return 0;
}

inline void free_expr(expr_tree_t *expr) {
}

inline char get_op_type(char *str) {
   char ndx1, ndx2;

   for (ndx1 = 0; ndx1 < NUM_OP_TYPES; ++ndx1)
      for (ndx2 = 0; ndx2 < NUM_OPS; ++ndx2)
         if (!strcmp(str, operators[ndx1][ndx2]))
            return ndx1 * NUM_OPS + ndx2;

   return -1;
}

void print_expr_node(node_t *nodes, int32_t index, int level) {
   int ndx;

   for (ndx = 0; ndx < level; ++ndx)
      printf("   ");

   if (nodes[index].type == OPERATOR) {
      printf("%s\n", operators[(int32_t)nodes[index].data / NUM_OPS]
            [(int32_t)nodes[index].data % NUM_OPS]);
      for (ndx = 0; ndx < ((nodes[index].data + 1) / NUM_OPS); ++ndx)
         print_expr_node(nodes, nodes[index].children[ndx], level+1);
   }
   else if (nodes[index].type == VARIABLE)
      printf("x%d\n", (int32_t)nodes[index].data);
   else if (nodes[index].type == CONSTANT)
      printf("%.2f\n", nodes[index].data);
   else printf("???\n");
}

void print_expr_tree(expr_tree_t *expr) {
   if (expr)
      print_expr_node(expr->nodes, expr->nodes[0].children[0], 0);
}

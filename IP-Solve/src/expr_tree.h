#ifndef EXPR_TREE_H
#define EXPR_TREE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "constants.h"

#define MAX_NUM_CHILDREN 2

#define CONSTANT 1
#define VARIABLE 2
#define OPERATOR 3

#define NUM_OP_TYPES 2
#define NUM_OPS 6

static const char *operators[NUM_OP_TYPES][NUM_OPS] = {
   // unary operators
   {
      "sin", "cos", "tan",
      "sec", "csc", "cot"
   },
   // binary operators
   {
      "+", "-", "*", "/",
      "^", "log"
   }
};

typedef struct node_t {
   int32_t parent, children[MAX_NUM_CHILDREN];
   unsigned char type;
   float data;
} node_t;

typedef struct expr_tree_t {
   node_t nodes[MAX_NUM_NODES + 1];
   uint16_t num_nodes;
} expr_tree_t;

char get_eq_expr(char *str, expr_tree_t *expr);
char get_ineq_expr(char *str, expr_tree_t *expr, unsigned char *cmp);
inline void free_expr(expr_tree_t *expr);
inline char get_op_type(char *str);
void print_expr_tree(expr_tree_t *expr);
unsigned char clear_expr(expr_tree_t *expr);

#ifdef __cplusplus
}
#endif

#endif

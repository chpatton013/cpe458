#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NUM_DIMS 3
#define MAX_NUM_NODES (5*(MAX_NUM_DIMS))

#define MAX_NUM_OBJS 1
#define MAX_NUM_CONS 1024

#define CMP_EQUAL_TO 1
#define CMP_LESS_THAN 2
#define CMP_GREATER_THAN 4

#define MAX_BLK_DIM 32768
#define TPB 128

#define DIMENSION_MSG "number of dimensions must be between 1 and %d, inclusive\n", MAX_NUM_DIMS

#ifdef __cplusplus
}
#endif

#endif

#include "constraint.h"

constraint_t * get_constraints(FILE *cons_fp, uint32_t *num_constraints) {
   constraint_t *cons;
   char *line, **strs;
   uint32_t ndx, num_strs, size, length;

   if (!(cons_fp && num_constraints)) return NULL;

   length = DFLT_NUM_CONS;

   CALL_ERR((strs = (char**)malloc(sizeof(char*) * DFLT_NUM_CONS)), NULL);
   for (ndx = 0; (line = read_long_line(cons_fp)); ++ndx) {
      if (ndx == length)
         CALL_ERR((strs = (char**)realloc(strs, sizeof(char*) *
               (length <<= 1))), NULL);

      strs[ndx] = line;
   }
   num_strs = ndx;

   CALL_ERR((cons = (constraint_t*)malloc(sizeof(constraint_t) * num_strs)),
         NULL);
   for (size = 0, ndx = 0; ndx < num_strs && size < MAX_NUM_CONS; ++ndx)
      if (get_ineq_expr(strs[ndx], &cons[ndx].expr, &cons[ndx].cmp))
         ++size;
   *num_constraints = size;

   for (ndx = 0; ndx < num_strs; ++ndx)
      if (strs[ndx]) free(strs[ndx]);
   free(strs);

   return cons;
}

inline void free_constraint(constraint_t *cons) {
}

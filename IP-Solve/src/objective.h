#ifndef OBJECTIVE_H
#define OBJECTIVE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <float.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "constants.h"
#include "expr_tree.h"

#define DFLT_NUM_OBJS 1024

typedef struct objective_t {
   expr_tree_t expr;
   float point[MAX_NUM_DIMS];
   volatile float value;
} objective_t;

objective_t * get_objectives(FILE *obj_fp, uint16_t *num_objectives);
inline void free_objective(objective_t *obj);

#ifdef __cplusplus
}
#endif

#endif

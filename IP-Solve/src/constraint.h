#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "constants.h"
#include "expr_tree.h"

#define DFLT_NUM_CONS 1024

typedef struct constraint_t {
   expr_tree_t expr;
   unsigned char cmp;
} constraint_t;

constraint_t * get_constraints(FILE *cons_fp, uint32_t *num_constraints);
inline void free_constraint(constraint_t *cons);

#ifdef __cplusplus
}
#endif

#endif

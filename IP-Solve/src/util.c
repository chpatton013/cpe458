#include "util.h"

char * read_long_line(FILE *fp) {
   char bf[DFLT_BF_SIZE];
   char *str;
   int ndx, size, length;

   if (!fp) return NULL;

   CALL_ERR((str = (char*)malloc(sizeof(char) * DFLT_STR_SIZE)), NULL);
   size = 0;
   length = DFLT_STR_SIZE;

   while (fgets(bf, DFLT_BF_SIZE, fp)) {
      for (ndx = 0; bf[ndx]; ++ndx) {
         if (size + 1 == length)
            CALL_ERR((str = (char*)realloc(str,
                  sizeof(char) * (length <<= 1))), NULL);

         str[size++] = bf[ndx];
      }

      if (str[size-1] == '\n') {
         str[size-1] = '\0';
         break;
      }
   }

   if (size == 0) {
      free(str);
      return NULL;
   }
   else {
      str[size] = '\0';
      return str;
   }
}

#ifndef IP_SOLVER_CUDA_H
#define IP_SOLVER_CUDA_H

#include <cuda.h>
#include <stdint.h>
#include <float.h>

#include "util.h"
#include "constants.h"
#include "expr_tree.h"
#include "expr_tree_cuda.cu"
#include "constraint.h"
#include "objective.h"
#include "result.h"

#define ulli unsigned long long int

extern uint16_t n;

#ifdef __cplusplus
extern "C" {

   __device__ static const uint64_t unlocked = 0xFFFFFFFFFFFFFFFF;

   /* Takes arrays of constraints, objectives, and boundaries; the number of
    * elements in the constraint and objective arrays; and a pointer to a
    * result.
    * Determines the maximum value of the given objectives and stores it in the
    * result.
    */
   void solve_cuda(constraint_t *cons, uint32_t num_constraints,
         objective_t *objs, uint16_t num_objectives, int32_t *bounds,
         result_t *res);

   /* Records the highest numbered variable in the given array of constraints.
    * Assumes a 2D grid layout and a 1D block layout.
    */
   __global__ void get_num_dims_cuda(constraint_t *d_cons,
         uint32_t num_constraints, uint16_t *num_dimensions);

   __global__ void init_global_memory(volatile uint16_t *d_max_ndx,
         objective_t *d_objs, volatile uint64_t *d_obj_lock,
         uint16_t num_objectives);

   /* Takes device arrays of constraints, objectives, and boundaries, the number
    * of elements in each, and a pointer to a result.
    * Checks every point in the n-dimensional space alloted by `d_bounds` for
    * the following:
    *    - in the feasible region
    *    - value of each objective function
    *    - new maximum value?
    * Determines the maximum value of the given objectives and stores it in the
    * result.
    * Assumes a 2D grid layout and a 1D block layout.
    */
   __global__ void optimize_cuda(constraint_t *d_cons, uint32_t num_constraints,
         objective_t *d_objs, volatile uint64_t *d_obj_lock,
         uint16_t num_objectives, int32_t *d_bounds, int32_t *d_ranges,
         uint32_t *d_partials, uint16_t num_dimensions,
         volatile uint16_t *d_max_ndx, uint32_t iter);
}
#endif

#endif

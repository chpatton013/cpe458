#include "ip_solver.h"

int main (int argc, char **argv) {
   uint32_t ndx;
   char *cons_file, *obj_file, *bound_file;
   FILE *cons_fp, *obj_fp, *bound_fp;

   cons_file = obj_file = bound_file = NULL;

   for (ndx = 1; ndx < argc; ++ndx) {
      if (!strstr(ARG_FLAGS, argv[ndx])) {
         printf(USAGE_MSG);
         exit(EXIT_FAILURE);
      }
      else if (!strcmp(argv[ndx], "c")) cons_file = argv[++ndx];
      else if (!strcmp(argv[ndx], "o")) obj_file = argv[++ndx];
      else bound_file = argv[++ndx];
   }

   if (!cons_file) cons_file = strdup(DFLT_CONS_FILE);
   if (!obj_file) obj_file = strdup(DFLT_OBJ_FILE);
   if (!bound_file) bound_file = strdup(DFLT_BOUND_FILE);

   CALL_ERR((cons_fp = fopen(cons_file, "r")), NULL);
   CALL_ERR((obj_fp = fopen(obj_file, "r")), NULL);
   CALL_ERR((bound_fp = fopen(bound_file, "r")), NULL);

   free(cons_file);
   free(obj_file);
   free(bound_file);

   CALL_N_ERR(ip_solve(cons_fp, obj_fp, bound_fp), 1);

   fclose(cons_fp);
   fclose(obj_fp);
   fclose(bound_fp);

   return 0;
}

char ip_solve(FILE *cons_fp, FILE *obj_fp, FILE *bound_fp) {
   int32_t boundaries[MAX_NUM_DIMS * 2];
   uint16_t num_objectives, max_ndx;
   uint32_t num_constraints;
   uint32_t ndx;
   constraint_t *constraints;
   objective_t *objectives;
   result_t result;

   CALL_ERR((constraints = get_constraints(cons_fp, &num_constraints)), NULL);
   CALL_ERR((objectives = get_objectives(obj_fp, &num_objectives)), NULL);
   CALL_ERR(get_boundaries(bound_fp, boundaries), 0);

   result.max_ndx = solve_cuda(constraints, num_constraints, objectives,
         num_objectives, boundaries, &result);
   result.obj = objectives + result.max_ndx;

   if (n > 0) {
      printf("function[%d](", result.max_ndx);
      for (ndx = 0; ndx < n-1; ++ndx)
         printf("%.0f,", result.obj->point[ndx]);
      printf("%.0f) = %f\n", result.obj->point[ndx], result.obj->value);
   }

   for (ndx = 0; ndx < num_constraints; ++ndx)
      free_constraint(&constraints[ndx]);
   free(constraints);

   for (ndx = 0; ndx < num_objectives; ++ndx)
      free_objective(&objectives[ndx]);
   free(objectives);

   return 1;
}

char get_boundaries(FILE *bound_fp, int32_t *boundaries) {
   char *line, *str, *err;
   uint16_t ndx;

   if (!bound_fp || !boundaries)
      return 0;

   ndx = 0;
   while ((line = read_long_line(bound_fp))) {
      str = strtok(line, " \n");
      for ( ; str && ndx < MAX_NUM_DIMS; ++ndx) {
         boundaries[ndx * 2] = (int32_t)strtof(str, &err);
         if (str == err) boundaries[ndx * 2] = 0;

         str = strtok(NULL, " \n");
         if (str) {
            boundaries[ndx * 2 + 1] = (int32_t)strtof(str, &err);
            if (str == err || boundaries[ndx * 2 + 1] < boundaries[ndx * 2])
               boundaries[ndx * 2 + 1] = boundaries[ndx * 2];

            str = strtok(NULL, " \n");
         }
         else boundaries[ndx * 2 + 1] = boundaries[ndx * 2];
      }
      free(line);
   }

   return 1;
}

#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <cuda.h>

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


static void dbg_print(const char *fmt, ...) {
   va_list args;
   va_start(args, fmt);
   vfprintf(stdout, fmt, args);
   va_end(args);
}

#ifdef DEBUG
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif

#ifndef DBG_PRINT
#define DBG_PRINT(fmt)                                                        \
   do { if (DEBUG_TEST) dbg_print fmt; } while (0)
#endif

static int64_t __util_call_val;

#ifndef CALL_ERR
#define CALL_ERR(funct, err) {                                                \
   __util_call_val = (int64_t)(funct);                                        \
   if (__util_call_val == (int64_t)(err)) {                                   \
      fprintf(stdout, "%s:%d: %lu\n", __FILE__, __LINE__, __util_call_val);   \
      exit(EXIT_FAILURE);                                                     \
   }                                                                          \
}
#endif

#ifndef CALL_N_ERR
#define CALL_N_ERR(funct, err) {                                              \
   __util_call_val = (int64_t)(funct);                                        \
   if (__util_call_val != (int64_t)(err)) {                                   \
      fprintf(stdout, "%s:%d: %lu\n", __FILE__, __LINE__, __util_call_val);   \
      exit(EXIT_FAILURE);                                                     \
   }                                                                          \
}
#endif

#ifndef CALL_WARN
#define CALL_WARN(funct, err) {                                               \
   __util_call_val = (int64_t)(funct);                                        \
   if (__util_call_val == (int64_t)(err)) {                                   \
      fprintf(stdout, "%s:%d: %lu\n", __FILE__, __LINE__, __util_call_val);   \
   }                                                                          \
}
#endif

#ifndef CALL_N_WARN
#define CALL_N_WARN(funct, err) {                                             \
   __util_call_val = (int64_t)(funct);                                        \
   if (__util_call_val != (int64_t)(err)) {                                   \
      fprintf(stdout, "%s:%d: %lu\n", __FILE__, __LINE__, __util_call_val);   \
   }                                                                          \
}
#endif

#define DFLT_BF_SIZE 512
#define DFLT_STR_SIZE 2048
#define DFLT_ARR_SIZE 128

char * read_long_line(FILE *fp);

#ifdef __cplusplus
}
#endif

#endif

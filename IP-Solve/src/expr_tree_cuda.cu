#ifndef EXPR_TREE_CUDA_CU
#define EXPR_TREE_CUDA_CU

#include "expr_tree_cuda.h"

#ifdef __cplusplus
extern "C" {
__device__ static float c_sin(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_cos(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_tan(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_sec(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_csc(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_cot(float *x, uint16_t num_args) {
   return 0;
}


__device__ static float c_plus(float *x, uint16_t num_args) {
   return num_args < 2 ? x[0] : x[0] + x[1];
}

__device__ static float c_minus(float *x, uint16_t num_args) {
   return num_args < 2 ? x[0] : x[0] - x[1];
}

__device__ static float c_mult(float *x, uint16_t num_args) {
   return num_args < 2 ? x[0] : x[0] * x[1];
}

__device__ static float c_div(float *x, uint16_t num_args) {
   return num_args < 2 ? x[0] : x[0] / x[1];
}

__device__ static float c_pow(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float c_log(float *x, uint16_t num_args) {
   return 0;
}

__device__ static float compute_node(node_t *nodes, int32_t index, float *x) {
   uint16_t ndx, op_type, op_ndx;
   float params[MAX_NUM_CHILDREN];

   if (nodes[index].type == CONSTANT) return nodes[index].data;
   else if (nodes[index].type == VARIABLE)
      return (int32_t)nodes[index].data < 1 ||
            (int32_t)nodes[index].data > MAX_NUM_DIMS ?
            0.0f : x[(int32_t)nodes[index].data - 1];
   else if (nodes[index].type == OPERATOR) {
      op_type = (uint32_t)nodes[index].data / NUM_OPS;
      op_ndx = (uint32_t)nodes[index].data % NUM_OPS;

      for (ndx = 0; ndx <= op_type && ndx < MAX_NUM_CHILDREN; ++ndx)
         params[ndx] = compute_node(nodes, nodes[index].children[ndx], x);

      return op_ptrs[op_type][op_ndx](params, ndx);
   }
   else return 0.0f;
}

__device__ static float compute_expr(expr_tree_t *expr, float *x) {
   if (!(expr && x)) return 0.0f;
   return compute_node(expr->nodes, expr->num_nodes, x);
}
}
#endif

#endif

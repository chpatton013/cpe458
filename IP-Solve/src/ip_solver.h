#ifndef IP_SOLVER_H
#define IP_SOLVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "constants.h"
#include "expr_tree.h"
#include "constraint.h"
#include "objective.h"
#include "result.h"
#include "ip_solver_cuda.h"

#define USAGE_MSG "usage: ip-solver [ c <constraint_file> ] [ o <objective_file> ] [ b <boundary_file> ]\n"
#define ARG_FLAGS "cob"

#define DFLT_CONS_FILE "./constraint_file.txt"
#define DFLT_OBJ_FILE "./objective_file.txt"
#define DFLT_BOUND_FILE "./boundary_file.txt"

uint16_t n;

char ip_solve(FILE *cons_fp, FILE *obj_fp, FILE *bound_fp);
char get_boundaries(FILE *bound_fp, int32_t *boundaries);

#ifdef __cplusplus
}
#endif

#endif

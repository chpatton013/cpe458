#ifndef EXPR_TREE_CUDA_H
#define EXPR_TREE_CUDA_H

#include <cuda.h>
#include <stdint.h>

#include "util.h"
#include "constants.h"
#include "expr_tree.h"

#ifdef __cplusplus
extern "C" {
__device__ static float c_sin(float *x, uint16_t num_args);
__device__ static float c_cos(float *x, uint16_t num_args);
__device__ static float c_tan(float *x, uint16_t num_args);
__device__ static float c_sec(float *x, uint16_t num_args);
__device__ static float c_csc(float *x, uint16_t num_args);
__device__ static float c_cot(float *x, uint16_t num_args);

__device__ static float c_plus(float *x, uint16_t num_args);
__device__ static float c_minus(float *x, uint16_t num_args);
__device__ static float c_mult(float *x, uint16_t num_args);
__device__ static float c_div(float *x, uint16_t num_args);
__device__ static float c_pow(float *x, uint16_t num_args);
__device__ static float c_log(float *x, uint16_t num_args);

__device__ static float (*op_ptrs[NUM_OP_TYPES][NUM_OPS])
      (float*, uint16_t) = {
   // unary operations
   {
      &c_sin, &c_cos, &c_tan,
      &c_sec, &c_csc, &c_cot
   },
   // binary operations
   {
      &c_plus, &c_minus, &c_mult,
      &c_div, &c_pow, &c_log
   }
};

__device__ static float compute_node(node_t *nodes, int32_t index, float *x);
__device__ static float compute_expr(expr_tree_t *expr, float *x);
}
#endif

#endif
